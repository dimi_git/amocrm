import './bootstrap';
import { createApp } from 'vue/dist/vue.esm-bundler';
const app = createApp({});

import {createRouter, createWebHistory} from 'vue-router'
import Home from './views/Home.vue';
import History from './views/History.vue';
import AddContact from './views/AddContact.vue';
const routes = [
    {
        path: '/',
        name: 'HomeLink',
        component: Home,
    },
    {
        path: '/history',
        name: 'HistoryLink',
        component: History,
    },
    {
        path: '/add-contact/:leadId',
        name: 'AddContactLink',
        component: AddContact,
    },
]

const router = createRouter({
    routes,
    history: createWebHistory(import.meta.env.VITE_BASE_URL || '/')
})

app.use(router).mount("#app")
