<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Document</title>
    @vite('resources/sass/app.scss')
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="collapse navbar-collapse px-5" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <router-link class="router-link" :to="{ name: 'HomeLink' }">Выбор сделки</router-link>
                </li>
                <li class="nav-item">
                    <router-link class="router-link" :to="{ name: 'HistoryLink' }">История</router-link>
                </li>
            </ul>
        </div>
    </nav>
    <router-view></router-view>
</div>
@vite('resources/js/app.js')
</body>
</html>
