<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Integration extends Model
{
    use HasFactory;

    protected $fillable = [
        'domain',
        'secret_key',
        'integration_id',
        'widget_id',
        'redirect_uri',
        'token',
    ];

    protected $casts = [
        'token'      => 'array',
    ];
}
