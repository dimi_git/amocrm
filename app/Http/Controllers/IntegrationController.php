<?php

namespace App\Http\Controllers;

use AmoCRM\Collections\CustomFieldsValuesCollection;
use AmoCRM\Collections\LinksCollection;
use AmoCRM\Collections\NotesCollection;
use AmoCRM\Exceptions\AmoCRMApiException;
use AmoCRM\Helpers\EntityTypesInterface;
use AmoCRM\Models\ContactModel;
use AmoCRM\Models\CustomFieldsValues\MultitextCustomFieldValuesModel;
use AmoCRM\Models\CustomFieldsValues\ValueCollections\MultitextCustomFieldValueCollection;
use AmoCRM\Models\CustomFieldsValues\ValueModels\MultitextCustomFieldValueModel;
use AmoCRM\Models\NoteType\CommonNote;
use App\Models\History;
use App\Models\Integration;
use App\Services\AmoCRMClientBuilder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class IntegrationController extends Controller
{

    public function index()
    {
        try {
            $integration = Integration::first();
            $apiClient = AmoCRMClientBuilder::create($integration);
            $leadsApi = $apiClient->leads()->get(null, ['contacts']);

            foreach ($leadsApi as $lead) {
                $leads[] = [
                    'id' => $lead->id,
                    'name' => $lead->name,
                    'created_at' => date('d.m.Y', $lead->created_at),
                    'contacts' => $lead->contacts ? 'Да' : 'Нет',
                ];
            }

            $leads = collect($leads)->sortBy('contacts', 1);

        } catch (AmoCRMApiException $e) {
            $leads = null;
        }

        return response()->json($leads);
    }

    public function history()
    {
            $historis = History::get();

            foreach ($historis as $history) {
                $history->date = $history->created_at->format('d.m.Y h:i');
            }

        return response()->json($historis);
    }

    public function addContact(Request $request)
    {
        $name = explode(" ", $request->name);
        $contact = new ContactModel();
        $contact->setFirstName(data_get($name, 0));
        $contact->setLastName(data_get($name, 1));
        $contact->setCustomFieldsValues(
            (new CustomFieldsValuesCollection())
                ->add(
                    (new MultitextCustomFieldValuesModel())
                        ->setFieldCode('PHONE')
                        ->setValues(
                            (new MultitextCustomFieldValueCollection())
                                ->add(
                                    (new MultitextCustomFieldValueModel())
                                        ->setValue($request->phone)
                                )
                        )
                )
        );

        $notesCollection = new NotesCollection();
        $commonNote = new CommonNote();
        $commonNote->setEntityId($request->lead_id);
        $commonNote->setText($request->note);
        $notesCollection->add($commonNote);

        try {
            $integration = Integration::first();
            $apiClient = AmoCRMClientBuilder::create($integration);
            $contactAdd = $apiClient->contacts()->addOne($contact);

            $lead = $apiClient->leads()->getOne($request->lead_id);
            $apiClient->leads()->link($lead, (new LinksCollection())->add($contactAdd));
            $apiClient->notes(EntityTypesInterface::LEADS)->add($notesCollection);

            History::create([
                'action' => 'Привязка контакта',
                'result' => 'Контакт привязан'
            ]);
        } catch (AmoCRMApiException $e) {
            History::create([
                'action' => 'Привязка контакта',
                'result' => $e->getMessage()
            ]);
        }

        return redirect('/');
    }

    public function amoAuth(Request $request): JsonResponse
    {
        $integration = Integration::where('integration_id', $request->client_id)->first();
        if ($integration)
            AmoCRMClientBuilder::auth($request->code, $request->referer, $integration);

        return response()->json();
    }
}
