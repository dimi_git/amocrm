<?php

namespace App\Services;

use AmoCRM\Client\AmoCRMApiClient;
use App\Models\Integration;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;

class AmoCRMClientBuilder
{
    static function auth(
        string $code,
        string $referer,
        Integration $integration
    ): AmoCRMApiClient
    {
        throw_if(empty($integration), 'RuntimeException: $integration is empty!');

        $amoClient = new AmoCRMApiClient(
            $integration->integration_id,
            $integration->secret_key,
            $integration->redirect_uri
        );
        $amoClient->setAccountBaseDomain($referer);
        $accessToken = $amoClient->getOAuthClient()->getAccessTokenByCode($code);
        $integration->update([
            'token' => [
                    'access_token'  => $accessToken->getToken(),
                    'refresh_token' => $accessToken->getRefreshToken(),
                    'expires'       => $accessToken->getExpires(),
                    'base_domain'   => $referer
                ]
        ]);

        return $amoClient;
    }

    static function create(Integration $integration): AmoCRMApiClient
    {
        throw_if(empty($integration), 'RuntimeException: $integration is empty!');
        $baseDomain = data_get($integration, 'token.base_domain');
        $accessToken = new AccessToken([
            'access_token'  => data_get($integration, 'token.access_token'),
            'refresh_token' => data_get($integration, 'token.refresh_token'),
            'expires'       => data_get($integration, 'token.expires'),
            'base_domain'   => $baseDomain,
        ]);
        $amoClient = new AmoCRMApiClient(
            $integration->integration_id,
            $integration->secret_key,
            $integration->redirect_uri
        );
        $amoClient->setAccessToken($accessToken)
            ->setAccountBaseDomain($baseDomain)
            ->onAccessTokenRefresh(
                static function(AccessTokenInterface $accessToken, string $baseDomain) use ($integration) {
                    $integration->update([
                        'token' => [
                                'access_token'  => $accessToken->getToken(),
                                'refresh_token' => $accessToken->getRefreshToken(),
                                'expires'       => $accessToken->getExpires(),
                                'base_domain'   => $baseDomain
                            ]
                    ]);
                }
            );

        return $amoClient;
    }
}
