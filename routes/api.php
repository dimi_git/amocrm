<?php

use App\Http\Controllers\IntegrationController;
use Illuminate\Support\Facades\Route;

Route::middleware('api')->get('/amo-auth', [IntegrationController::class, 'amoAuth']);
