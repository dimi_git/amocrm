<?php

use App\Http\Controllers\IntegrationController;
use Illuminate\Support\Facades\Route;



Route::middleware('web')->group(function() {

    Route::get('{any}', function () {
        return view('app');
    })->where('any', '.*');

    Route::post('/data/home', [IntegrationController::class, 'index'])->name('home');
    Route::post('/data/history', [IntegrationController::class, 'history'])->name('history');
    Route::post('/data/add-contact', [IntegrationController::class, 'addContact'])->name('addContact');
});
